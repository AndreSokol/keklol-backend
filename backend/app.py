from aiohttp import web
import asyncpg

from backend import handlers


class BCApp(web.Application):
    pg_pool: asyncpg.pool.Pool
    pg_dsn: str

    def __init__(self, pg_dsn=None, **kwargs):
        super().__init__(**kwargs)
        self.pg_dsn = pg_dsn

        self.on_startup.append(self._init_pg_pool)
        self.on_shutdown.append(self._close_pg_pool)

    async def _init_pg_pool(self, *args, **kwargs):
        self.pg_pool = await asyncpg.pool.create_pool(self.pg_dsn)

    async def _close_pg_pool(self, *args, **kwargs):
        await self.pg_pool.close()


@web.middleware
async def cors_middleware(request, handler):
    response: web.Response = await handler(request)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    return response


async def options(request):
    return web.Response()


def create_app(pg_dsn=None) -> BCApp:
    app = BCApp(pg_dsn=pg_dsn, middlewares=[cors_middleware])

    app.router.add_options('/{tail:.*}', options)
    app.router.add_get('/current/', handlers.get_current)
    app.router.add_post('/increase/', handlers.increase)

    return app
