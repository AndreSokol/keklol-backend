from aiohttp import web
import asyncpg


async def get_current(request: web.Request):
    pg_pool: asyncpg.pool.Pool = request.app.pg_pool

    async with pg_pool.acquire() as conn:
        conn: asyncpg.connection.Connection
        counter_rows = await conn.fetch('SELECT id, cnt FROM counters')

    return web.json_response({row['id']: row['cnt'] for row in counter_rows})


async def increase(request: web.Request):
    pg_pool: asyncpg.pool.Pool = request.app.pg_pool
    counter_id = (await request.json())['id']

    async with pg_pool.acquire() as conn:
        conn: asyncpg.connection.Connection
        await conn.fetch('UPDATE counters SET cnt = cnt + 1 WHERE id = $1', counter_id)
        counter_rows = await conn.fetch('SELECT id, cnt FROM counters')

    return web.json_response({row['id']: row['cnt'] for row in counter_rows})
