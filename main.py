import asyncio
from aiohttp import web
import os
import uvloop

from backend import app

if __name__ == '__main__':
    pg_dsn = os.getenv('DATABASE_URL', 'postgres://andresokol:andresokol@localhost:5432/keklol')
    port = os.getenv('PORT', 8080)

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    instance = app.create_app(pg_dsn=pg_dsn)
    web.run_app(app=instance, port=port)
